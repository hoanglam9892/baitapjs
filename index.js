/*
 bai tap so 1: tinh tien luong nhan vien

input: so ngay lam ( vd 30 ngay)

xu ly: lay luong 1 ngay = 100.000 vnd * so ngay lam

output: so tien luong cua nhan vien dua tren so ngay lam

Code:
*/
const luong_1_ngay = 100000;
var so_ngay_lam = 30;
console.log(luong_nhan_vien = so_ngay_lam * luong_1_ngay + " VNĐ");


/*

Bai tap so 2: tinh gia tri trung binh

input: 5 so thuc

xu ly: tổng 5 số rồi chia 5

output: gia tri trung binh cua 5 so thuc

Code:

*/

var so_1 = 1.1;
var so_2 = 1.2;
var so_3 = 1.3;
var so_4 = 1.4;
var so_5 = 1.5;
console.log(trungbinh = (so_1 + so_2 + so_3 + so_4 + so_5)/5);

/* 
Bai tap so 3: quy doi tien

input: so usd ( vd 100 usd)

xu ly: lay so usd * 23500 vnd.

output: so tien vnd

Code:

*/

const usd = 23500;
var so_luong_usd = 100;
console.log (vnd= so_luong_usd*usd + " VNĐ");


/* 
Bai tap so 4: tinh dien tich, chu vi hinh chu nhat 

input: chieu dai, chieu rong (vd chieu dai = 5, chieu rong = 3)

xu ly: 

chu vi = 2 * ( chieu dai + chieu rong )

dien tich = chieu dai * chieu rong

output: chu vi hinh chu nhat + dien tich hinh chu nhat

Code:

*/


var chieudaihcn = 5;
var chieuronghcn = 3; 
console.log("Chu vi hinh chu nhat la", chuvihcn =  2 * (chieudaihcn + chieuronghcn));
console.log("Dien tich hinh chu nhat la ", dientichhcn = chieudaihcn * chieuronghcn );


/* 
Bai tap so 5: tinh tong 2 ki so

input: 1 so co 2 chu so (vd 44)

xu ly: 

lay so hang don vi = so % 10
lay so hang chuc = so /10

output: chu vi hinh chu nhat + dien tich hinh chu nhat

Code:

*/

var num= 44;
var lastnumber= num % 10; // 4
var firstnumber= Math.floor(num / 10); //4
console.log("Tong cua 2 ki so la", firstnumber + lastnumber);